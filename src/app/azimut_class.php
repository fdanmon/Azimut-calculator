<?php

define("EARTH_RADIUS", 6378.14); //in Km
define("SAT_EARTH_RADIUS", 42164.57); //in Km


class Azimut
{
	//Three properties are in degrees
	protected $lat_station; 
	protected $lon_station;
	protected $lon_satellite;

	public function __construct($lat_st, $lon_st, $lon_sat){
		$this->lat_station = $lat_st;
		$this->lon_station = $lon_st;
		$this->lon_satellite = $lon_sat;
	}



	public function get_gamma(){
	  	/*
	  	* This method returns gamma parameter in radians.
	  	To calculate it we need station latitude and longitude
		and the satellite longitude in radians, too.
	  	*/
	  return round(acos(cos(deg2rad($this->lat_station))*cos(deg2rad(abs($this->lon_satellite - $this->lon_station)))), 3);
	}

	public function get_elevation(){
		/*
		* This method returns the elevation angle in degrees.
		* Gamma parameter must be in radians
		*/
		return round(rad2deg(atan((6.61075 - cos($this->get_gamma()))/sin($this->get_gamma())) - $this->get_gamma()), 3);
	}

	public function get_distance(){
		/*
		* This method returns the distance between the earth station and the satellite.
		* Distance will return in Km, and we need the gamma parameter to calculate it.
		*/
		return round(SAT_EARTH_RADIUS*sqrt(1 + pow((EARTH_RADIUS/SAT_EARTH_RADIUS), 2) - 2*(EARTH_RADIUS/SAT_EARTH_RADIUS)*cos($this->get_gamma())), 3);
	}

	public function get_phi(){
		/*
	  	* This method returns phi parameter in radians, which
	  	could help us to calculate azimut angle.
	  	*/
	  	return round(atan(tan(deg2rad(abs($this->lon_satellite - $this->lon_station)))/sin(deg2rad($this->lat_station))), 3);
	}

	public function get_azimut(){
		/*
		* This function returns the azimut angle, in degrees °N.
		We'll need the phi parameter.
		*/

		$this_azimut;

		if($this->lon_satellite < $this->lon_station && $this->lat_station>0){
			$this_azimut = round(180 + rad2deg($this->get_phi()), 3);
		}
		else if($this->lon_satellite > $this->lon_station && $this->lat_station>0){
			$this_azimut = round(180 - rad2deg($this->get_phi()), 3);
		}
		else if($this->lon_satellite > $this->lon_station && $this->lat_station<0){
			$this_azimut = round(rad2deg($this->get_phi()), 3);
		}
		else if($this->lon_satellite < $this->lon_station && $this->lat_station<0){
			$this_azimut = round(360 - rad2deg($this->get_phi()), 3);
		}

		return $this_azimut;
	}

}


//Test
$azim = new Azimut(40.4, -3.68, -30);

echo round($azim->get_gamma(), 3) . "<br/>";
echo $azim->get_elevation() . "<br/>";
echo $azim->get_distance() . "<br/>";
echo round(rad2deg($azim->get_phi()), 3) . "<br/>";
echo $azim->get_azimut() . "<br/>";



?>
