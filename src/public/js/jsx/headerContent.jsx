class HeaderPage extends React.Component
{
	constructor(props){
		super(props);
	}

	render(){
		return (
			<div className="row justify-content-between">
				<div className="col-12 col-md-5">
					<h1 className="header-font">{this.props.title}</h1>
				</div>
				<div className="col-12 col-md-3">
					<a href="https://universidadvirtualdelperu.edu.pe/calculator" className="block-anchor">Ir a Satelite</a>
				</div>
			</div>
		);
	}
}

ReactDOM.render(
	<HeaderPage title="Azimut"/>,
	document.getElementById("headerContent")
);
