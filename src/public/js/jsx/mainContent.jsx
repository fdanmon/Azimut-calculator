const earthStation = {
  type: "earthbound",
  description_es: "Estación Terrestre",
  data: [
          {
            name: "location_st",
            label_es: "Nombre",
            numeric: false
          },
          {
            name: "latitude_st",
            label_es: "Latitud",
            numeric: true,
            unit: "degrees",
            unitLabel: "°N"
          },
          {
            name: "longitude_st",
            label_es: "Longitud",
            numeric: true,
            unit: "degrees",
            unitLabel: "°E"
          }
    ]
};

const satelite = {
  type: "satelite",
  description_es: "Satélite",
  data: [
    {
      name: "location_sat",
      label_es: "Nombre",
      numeric: false
    },
    {
      name: "longitude_sat",
      label_es: "Longitud",
      numeric: true,
      unit: "degrees",
      unitLabel: "°E"
    }
  ]
};



class InputField extends React.Component
{

  constructor(props){
    super(props);
    this.validateChars = this.validateChars.bind(this);
  }

  validateChars(e){
    let theEvent = e || window.event;
    let key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    const regex = this.props.data.numeric ? /[0-9]|\.|\-|[\b]/ : /[A-Za-z]|\d|\-|[\b]|\.|\s/;
    if(!key.match(regex)){ //it could also be regex.test(key)
      theEvent.returnValue = false;
      if(!theEvent.defaultPrevented){
        theEvent.preventDefault();
      }
    }
  }

  renderInput(){
    return(
      <input type="text" class="form-control" name={this.props.data.name} id={this.props.data.name} placeholder={this.props.data.label_es} aria-label={this.props.data.label_es} onKeyPress={this.validateChars}/>
    );
  }

  render(){
    if(!this.props.data.numeric){
      return (
        <div className="input-group margin-input">
          {this.renderInput()}
        </div>
      );
    }
    else {
      return (
        <div className="input-group margin-input">
          {this.renderInput()}
          <div class="input-group-append">
            <span className="input-group-text">{this.props.data.unitLabel}</span>
          </div>
        </div>
      );
    }
  }


}



class TableForm extends React.Component
{
  render(){
    return (
      	<div className="table-form">
  			<div className="table-form-title">
  				<h3>{this.props.thisData.description_es}</h3>
  			</div>
  			<div className="table-form-inputs">
  				{this.props.thisData.data.map((eData, i) => <InputField data={eData}/>)}
  			</div>
  		</div>
    );
  }
}



ReactDOM.render(
	<TableForm thisData={earthStation}/>,
	document.getElementById("earth-station")
);

ReactDOM.render(
	<TableForm thisData={satelite}/>,
	document.getElementById("satelite")
);

ReactDOM.render(
	<input className="btn btn-info" type="submit" value="¡Calcular!"/>,
	document.getElementById("submit")
);
