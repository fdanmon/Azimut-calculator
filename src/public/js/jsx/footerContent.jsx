const dataCompany = {
	name: "Universidad virtual del Perú",
	adress: {
		name: "Calle Juvenal Denegri 215 Santa Catalina, Lima 13.",
		url: "https:\/\/www.google.com/maps/place/Juvenal+Denegri+215,+Cercado+de+Lima+15034/@-12.0838197,-77.0133461,17z/data=!3m1!4b1!4m5!3m4!1s0x9105c88010119123:0xdfb6e87f2a1975ad!8m2!3d-12.0838197!4d-77.0111574"
	},
	phone: "(01) 375 7903"
};


class FooterPage extends React.Component
{
	constructor(props){
		super(props);
	}

	render(){
		return (
			<div className="col-12 col-sm-10 col-md-6">
				<div className="footer-item">
					<adress><a href="https://universidadvirtualdelperu.edu.pe" target="_blank" className="block-anchor">{this.props.data.name}</a></adress>
				</div>
				<div className="footer-item">
					<adress><a href={this.props.data.adress.url} target="_blank" className="block-anchor">{this.props.data.adress.name}</a></adress>
				</div>
				<div className="footer-item">
					<adress>{this.props.data.phone}</adress>
				</div>
			</div>
		);
	}
}


ReactDOM.render(
	<FooterPage data={dataCompany}/>,
	document.getElementById("footer-content")
);
